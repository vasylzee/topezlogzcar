<?php
/**
 * TeC functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package TeC
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'tec_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tec_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on TeC, use a find and replace
		 * to change 'tec' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tec', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'tec' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'tec_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'tec_setup' );

function tec_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tec_content_width', 640 );
}
add_action( 'after_setup_theme', 'tec_content_width', 0 );


function tec_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'tec' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'tec' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'tec_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tec_scripts() {
	wp_enqueue_style( 'tec-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'tec-style', 'rtl', 'replace' );

	wp_enqueue_script( 'tec-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tec_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_action( 'wp_enqueue_scripts', 'style_theme' );
add_action( 'after_setup_theme', 'theme_register_nav_menu');
add_action( 'wp_footer', 'scripts_theme');
add_action( 'init', 'register_cars_post_type' );
add_action( 'widgets_init', 'register_tec_widgets');


add_filter('wp_nav_menu','add_menuclass');
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);



function register_tec_widgets(){
	register_sidebar( array(
		'name'          => 'Left Sidebar',
		'id'            => "left-sitebar",
		'description'   => '',
		'class'         => '',
		'before_widget' => '<ul id="%1$s" class="widget_recent_post %2$s">',
		'after_widget'  => "</ul>\n",
		'before_title'  => '<h2 class="widget_title">',
		'after_title'   => "</h2>\n",
	) );
}

function theme_register_nav_menu(){
    register_nav_menu( 'top', 'header_menu');
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails'); 
}


function style_theme(){

    wp_enqueue_style('style', get_stylesheet_uri());
    // <!-- Animation CSS -->
    wp_enqueue_style('style-animate', get_template_directory_uri() . '/assets/css/animate.css');
    // <!-- Latest Bootstrap min CSS -->
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');
    // <!-- Icon Font CSS -->
    wp_enqueue_style('style-min', get_template_directory_uri() . '/assets/css/all.min.css');
    wp_enqueue_style('style-ionicons', get_template_directory_uri() . '/assets/css/ionicons.min.css');
    wp_enqueue_style('style-themify', get_template_directory_uri() . '/assets/css/themify-icons.css');
    wp_enqueue_style('style-linearicons', get_template_directory_uri() . '/assets/css/linearicons.css');
    
    wp_enqueue_style('style-carousel', get_template_directory_uri() . '/assets/owlcarousel/css/owl.carousel.min.css');
    wp_enqueue_style('style-carousel-theme', get_template_directory_uri() . '/assets/owlcarousel/css/owl.theme.css');
    wp_enqueue_style('style-carousel-theme-default', get_template_directory_uri() . '/assets/owlcarousel/css/owl.theme.default.min.css');
    // <!-- Slick CSS -->
    wp_enqueue_style('style-slick', get_template_directory_uri() . '/assets/css/slick.css');
    wp_enqueue_style('style-slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css');
    // <!-- Magnific Popup CSS -->
    wp_enqueue_style('style-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css');
    // <!-- preloader --> 
    wp_enqueue_style('preloader', get_template_directory_uri() . '/assets/css/preloader.css');

    // <!-- Style CSS -->
    wp_enqueue_style('style-bbpress', get_template_directory_uri() . '/assets/css/style.css');
    wp_enqueue_style('style-responsive', get_template_directory_uri() . '/assets/css/responsive.css');
    wp_enqueue_style('style-theme-red', get_template_directory_uri() . '/assets/color/theme-red.css');
}

function scripts_theme(){
    // <!-- Latest jQuery --> 
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery-1.12.4.min.js' );
    // <!-- Latest compiled and minified Bootstrap --> 
    wp_enqueue_script('scripts-bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js' );
    // <!-- owl-carousel min js  --> 
    wp_enqueue_script('scripts-carousel', get_template_directory_uri() . '/assets/owlcarousel/js/owl.carousel.min.js' );
    // <!-- magnific-popup min js  --> 
    wp_enqueue_script('scripts-magnific', get_template_directory_uri() . '/assets/js/magnific-popup.min.js' );
    // <!-- waypoints min js  --> 
    wp_enqueue_script('scripts-waypoints', get_template_directory_uri() . '/assets/js/waypoints.min.js' );
    // <!-- parallax js  --> 
    wp_enqueue_script('scripts-parallax', get_template_directory_uri() . '/assets/js/parallax.js' );
    // <!-- countdown js  --> 
    wp_enqueue_script('scripts-countdown', get_template_directory_uri() . '/assets/js/jquery.countdown.min.js',['jquery'],);
    // <!-- jquery.countTo js  -->
    wp_enqueue_script('scripts-countTo', get_template_directory_uri() . '/assets/js/jquery.countTo.js',['jquery'],);
    // <!-- imagesloaded js --> 
    wp_enqueue_script('scripts-imagesloaded', get_template_directory_uri() . '/assets/js/imagesloaded.pkgd.min.js' );
    // <!-- isotope min js --> 
    wp_enqueue_script('scripts-isotope', get_template_directory_uri() . '/assets/js/isotope.min.js' );
    // <!-- jquery.appear js  -->
    wp_enqueue_script('scripts-appear', get_template_directory_uri() . '/assets/js/jquery.appear.js',['jquery'],);
    // <!-- jquery.dd.min js -->
    wp_enqueue_script('scripts-dd', get_template_directory_uri() . '/assets/js/jquery.dd.min.js',['jquery'],);
    // <!-- slick js -->
    wp_enqueue_script('scripts-slick', get_template_directory_uri() . '/assets/js/slick.min.js' );
    // <!-- DatePicker js -->
    wp_enqueue_script('scripts-datepicker', get_template_directory_uri() . '/assets/js/datepicker.min.js' );
   
    // <!-- scripts js --> 
    wp_enqueue_script('scripts-main', get_template_directory_uri() . '/assets/js/scripts.js' );
}

function add_menuclass($ulclass) {
    return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
 }

 function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}


function register_cars_post_type() {
    register_taxonomy('car-brand', array('cars'), array(
         'label'                 => '',
         'labels'                => array(
             'name'              => 'Brands',
             'singular_name'     => 'Brand',
             'search_items'      => 'Search Brand',
         ),
         'description'           => 'Brands',
         'public'                => true,
         'show_in_nav_menus'     => true,
         'show_ui'               => true,
         'show_tagcloud'         => false,
         'hierarchical'          => true,
         'rewrite'               => array('slug'=>'car-brand', 'hierarchical'=>true),
         'show_admin_column'     => true,
    ) );
    register_taxonomy('car-type', array('cars'), array(
        'label'                 => 'Cars',
        'labels'                => array(
            'name'              => 'Types',
            'singular_name'     => 'Type',
            'search_items'      => 'Search Type',
            'all_items'         => 'All Types',
			'view_item '        => 'View Type',
			'parent_item'       => 'Parent Type',
			'parent_item_colon' => 'Parent Type:',
			'edit_item'         => 'Edit Type',
			'update_item'       => 'Update Type',
			'add_new_item'      => 'Add New Type',
			'new_item_name'     => 'New Type Name',
			'menu_name'         => 'Type',
        ),
        'description'           => 'Types',
        'public'                => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => false,
        'hierarchical'          => true,
        'rewrite'               => array('slug'=>'car-type', 'hierarchical'=>true),
        'show_admin_column'     => true,
    ) );
    register_taxonomy('car-year', array('cars'), array(
        'label'                 => 'Cars',
        'labels'                => array(
            'name'              => 'Years',
            'singular_name'     => 'Year',
            'search_items'      => 'Search Year',
            'all_items'         => 'All Years',
			'view_item '        => 'View Year',
			'parent_item'       => 'Parent Year',
			'parent_item_colon' => 'Parent Year:',
			'edit_item'         => 'Edit Year',
			'update_item'       => 'Update Year',
			'add_new_item'      => 'Add New Year',
			'new_item_name'     => 'New Year Name',
			'menu_name'         => 'Year',
        ),
        'description'           => 'Years cars', 
        'public'                => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => false,
        'hierarchical'          => true,
        'rewrite'               => array('slug'=>'car-year' , 'hierarchical'=>false),
        'show_admin_column'     => true,
    ) );
    register_post_type('cars', array(
        'label'       => 'Cars',
        'labels'      => array(
            'name'               => 'Cars',
            'singular_name'      => 'Cars',
            'menu_name'          => 'Cars',
            'all_items'          => 'All Cars',
            'add_new'            => 'Add Car',
            'add_new_item'       => 'Add Card Car', 
		    'edit_item'          => 'Edit Car',
		    'new_item'           => 'New Car',
		    'view_item'          => 'View Car',
		    'search_items'       => 'Search Car',
		    'not_found'          => 'Not found',
		    'not_found_in_trash' => 'Not found in trash',
        ),
        'description'         => '',
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-car',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'rewrite'             => array( 'slug'=>'cars/%car-brand%'),
        'has_archive'         => 'cars',
        'query_var'           => true,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'comments'),
        'taxonomies'          => array( 'car-brand', 'car-type', 'car-year'),
    ) );
}
add_filter('post_type_link', 'cars_permalink', 1, 2);
function cars_permalink( $permalink, $post ){
    if( strpos($permalink, '%car-brand%') === false )
        return $permalink;
    $terms = get_the_terms($post, 'car-brand');
    $parent = $terms[0]->parent;
    if( ! is_wp_error($terms) && !empty($terms) && is_object($terms[0]) ) {
        $term_slug = array_pop($terms)->slug;
    }
    else
        $term_slug = 'no-car-brand';
    return str_replace('%car-brand%', $term_slug, $permalink );
}

 


 /**
  * Хлебные крошки для WordPress (breadcrumbs)
  *
  * @param  string [$sep  = '']      Разделитель. По умолчанию ' » '
  * @param  array  [$l10n = array()] Для локализации. См. переменную $default_l10n.
  * @param  array  [$args = array()] Опции. См. переменную $def_args
  * @return string Выводит на экран HTML код
  *
  * version 3.3.2
  */
 function kama_breadcrumbs( $sep = ' > ', $l10n = array(), $args = array() ){
     $kb = new Kama_Breadcrumbs;
     echo $kb->get_crumbs( $sep, $l10n, $args );
 }
 
 class Kama_Breadcrumbs {
 
     public $arg;
 
     // Локализация
     static $l10n = array(
         'home'       => 'Home',
         'paged'      => 'page %d',
         '_404'       => 'error 404',
         'search'     => 'Search results - <b>%s</b>',
         'author'     => 'Author archive: <b>%s</b>',
         'year'       => 'Archive for: <b>%d</b> the year',
         'month'      => 'Archive for: <b>%s</b>',
         'day'        => '',
         'attachment' => 'Media: %s',
         'tag'        => 'Records by tag: <b>%s</b>',
         'tax_tag'    => '%1$s of "%2$s" by tag: <b>%3$s</b>',
         // tax_tag выведет: 'тип_записи из "название_таксы" по тегу: имя_термина'.
         // Если нужны отдельные холдеры, например только имя термина, пишем так: 'записи по тегу: %3$s'
     );
 
     // Параметры по умолчанию
     static $args = array(
         'on_front_page'   => true,  // выводить крошки на главной странице
         'show_post_title' => true,  // показывать ли название записи в конце (последний элемент). Для записей, страниц, вложений
         'show_term_title' => true,  // показывать ли название элемента таксономии в конце (последний элемент). Для меток, рубрик и других такс
         'title_patt'      => '<span class="kb_title">%s</span>', // шаблон для последнего заголовка. Если включено: show_post_title или show_term_title
         'last_sep'        => true,  // показывать последний разделитель, когда заголовок в конце не отображается
         'markup'          => 'schema.org', // 'markup' - микроразметка. Может быть: 'rdf.data-vocabulary.org', 'schema.org', '' - без микроразметки
                                            // или можно указать свой массив разметки:
                                            // array( 'wrappatt'=>'<div class="kama_breadcrumbs">%s</div>', 'linkpatt'=>'<a href="%s">%s</a>', 'sep_after'=>'', )
         'priority_tax'    => array('category'), // приоритетные таксономии, нужно когда запись в нескольких таксах
         'priority_terms'  => array(), // 'priority_terms' - приоритетные элементы таксономий, когда запись находится в нескольких элементах одной таксы одновременно.
                                       // Например: array( 'category'=>array(45,'term_name'), 'tax_name'=>array(1,2,'name') )
                                       // 'category' - такса для которой указываются приор. элементы: 45 - ID термина и 'term_name' - ярлык.
                                       // порядок 45 и 'term_name' имеет значение: чем раньше тем важнее. Все указанные термины важнее неуказанных...
         'nofollow' => false, // добавлять rel=nofollow к ссылкам?
 
         // служебные
         'sep'             => '',
         'linkpatt'        => '',
         'pg_end'          => '',
     );
 
     function get_crumbs( $sep, $l10n, $args ){
         global $post, $wp_query, $wp_post_types;
 
         self::$args['sep'] = $sep;
 
         // Фильтрует дефолты и сливает
         $loc = (object) array_merge( apply_filters('kama_breadcrumbs_default_loc', self::$l10n ), $l10n );
         $arg = (object) array_merge( apply_filters('kama_breadcrumbs_default_args', self::$args ), $args );
 
         $arg->sep = '<span class="kb_sep">'. $arg->sep .'</span>'; // дополним
 
         // упростим
         $sep = & $arg->sep;
         $this->arg = & $arg;
 
         // микроразметка ---
         if(1){
             $mark = & $arg->markup;
 
             // Разметка по умолчанию
             if( ! $mark ) $mark = array(
                 'wrappatt'  => '<div class="kama_breadcrumbs">%s</div>',
                 'linkpatt'  => '<a href="%s">%s</a>',
                 'sep_after' => '',
             );
             // rdf
             elseif( $mark === 'rdf.data-vocabulary.org' ) $mark = array(
                 'wrappatt'   => '<div class="kama_breadcrumbs" prefix="v: http://rdf.data-vocabulary.org/#">%s</div>',
                 'linkpatt'   => '<span typeof="v:Breadcrumb"><a href="%s" rel="v:url" property="v:title">%s</a>',
                 'sep_after'  => '</span>', // закрываем span после разделителя!
             );
             // schema.org
             elseif( $mark === 'schema.org' ) $mark = array(
                 'wrappatt'   => '<div class="kama_breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">%s</div>',
                 'linkpatt'   => '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="%s" itemprop="item"><span itemprop="name">%s</span></a></span>',
                 'sep_after'  => '',
             );
 
             elseif( ! is_array($mark) )
                 die( __CLASS__ .': "markup" parameter must be array...');
 
             $wrappatt  = $mark['wrappatt'];
             $arg->linkpatt  = $arg->nofollow ? str_replace('<a ','<a rel="nofollow"', $mark['linkpatt']) : $mark['linkpatt'];
             $arg->sep      .= $mark['sep_after']."\n";
         }
 
         $linkpatt = $arg->linkpatt; // упростим
 
         $q_obj = get_queried_object();
 
         // может это архив пустой таксы?
         $ptype = null;
         if( empty($post) ){
             if( isset($q_obj->taxonomy) )
                 $ptype = & $wp_post_types[ get_taxonomy($q_obj->taxonomy)->object_type[0] ];
         }
         else $ptype = & $wp_post_types[ $post->post_type ];
 
         // paged
         $arg->pg_end = '';
         if( ($paged_num = get_query_var('paged')) || ($paged_num = get_query_var('page')) )
             $arg->pg_end = $sep . sprintf( $loc->paged, (int) $paged_num );
 
         $pg_end = $arg->pg_end; // упростим
 
         $out = '';
 
         if( is_front_page() ){
             return $arg->on_front_page ? sprintf( $wrappatt, ( $paged_num ? sprintf($linkpatt, get_home_url(), $loc->home) . $pg_end : $loc->home ) ) : '';
         }
         // страница записей, когда для главной установлена отдельная страница.
         elseif( is_home() ) {
             $out = $paged_num ? ( sprintf( $linkpatt, get_permalink($q_obj), esc_html($q_obj->post_title) ) . $pg_end ) : esc_html($q_obj->post_title);
         }
         elseif( is_404() ){
             $out = $loc->_404;
         }
         elseif( is_search() ){
             $out = sprintf( $loc->search, esc_html( $GLOBALS['s'] ) );
         }
         elseif( is_author() ){
             $tit = sprintf( $loc->author, esc_html($q_obj->display_name) );
             $out = ( $paged_num ? sprintf( $linkpatt, get_author_posts_url( $q_obj->ID, $q_obj->user_nicename ) . $pg_end, $tit ) : $tit );
         }
         elseif( is_year() || is_month() || is_day() ){
             $y_url  = get_year_link( $year = get_the_time('Y') );
 
             if( is_year() ){
                 $tit = sprintf( $loc->year, $year );
                 $out = ( $paged_num ? sprintf($linkpatt, $y_url, $tit) . $pg_end : $tit );
             }
             // month day
             else {
                 $y_link = sprintf( $linkpatt, $y_url, $year);
                 $m_url  = get_month_link( $year, get_the_time('m') );
 
                 if( is_month() ){
                     $tit = sprintf( $loc->month, get_the_time('F') );
                     $out = $y_link . $sep . ( $paged_num ? sprintf( $linkpatt, $m_url, $tit ) . $pg_end : $tit );
                 }
                 elseif( is_day() ){
                     $m_link = sprintf( $linkpatt, $m_url, get_the_time('F'));
                     $out = $y_link . $sep . $m_link . $sep . get_the_time('l');
                 }
             }
         }
         // Древовидные записи
         elseif( is_singular() && $ptype->hierarchical ){
             $out = $this->_add_title( $this->_page_crumbs($post), $post );
         }
         // Таксы, плоские записи и вложения
         else {
             $term = $q_obj; // таксономии
 
             // определяем термин для записей (включая вложения attachments)
             if( is_singular() ){
                 // изменим $post, чтобы определить термин родителя вложения
                 if( is_attachment() && $post->post_parent ){
                     $save_post = $post; // сохраним
                     $post = get_post($post->post_parent);
                 }
 
                 // учитывает если вложения прикрепляются к таксам древовидным - все бывает :)
                 $taxonomies = get_object_taxonomies( $post->post_type );
                 // оставим только древовидные и публичные, мало ли...
                 $taxonomies = array_intersect( $taxonomies, get_taxonomies( array('hierarchical' => true, 'public' => true) ) );
 
                 if( $taxonomies ){
                     // сортируем по приоритету
                     if( ! empty($arg->priority_tax) ){
                         usort( $taxonomies, function($a,$b)use($arg){
                             $a_index = array_search($a, $arg->priority_tax);
                             if( $a_index === false ) $a_index = 9999999;
 
                             $b_index = array_search($b, $arg->priority_tax);
                             if( $b_index === false ) $b_index = 9999999;
 
                             return ( $b_index === $a_index ) ? 0 : ( $b_index < $a_index ? 1 : -1 ); // меньше индекс - выше
                         } );
                     }
 
                     // пробуем получить термины, в порядке приоритета такс
                     foreach( $taxonomies as $taxname ){
                         if( $terms = get_the_terms( $post->ID, $taxname ) ){
                             // проверим приоритетные термины для таксы
                             $prior_terms = & $arg->priority_terms[ $taxname ];
                             if( $prior_terms && count($terms) > 2 ){
                                 foreach( (array) $prior_terms as $term_id ){
                                     $filter_field = is_numeric($term_id) ? 'term_id' : 'slug';
                                     $_terms = wp_list_filter( $terms, array($filter_field=>$term_id) );
 
                                     if( $_terms ){
                                         $term = array_shift( $_terms );
                                         break;
                                     }
                                 }
                             }
                             else
                                 $term = array_shift( $terms );
 
                             break;
                         }
                     }
                 }
 
                 if( isset($save_post) ) $post = $save_post; // вернем обратно (для вложений)
             }
 
             // вывод
 
             // все виды записей с терминами или термины
             if( $term && isset($term->term_id) ){
                 $term = apply_filters('kama_breadcrumbs_term', $term );
 
                 // attachment
                 if( is_attachment() ){
                     if( ! $post->post_parent )
                         $out = sprintf( $loc->attachment, esc_html($post->post_title) );
                     else {
                         if( ! $out = apply_filters('attachment_tax_crumbs', '', $term, $this ) ){
                             $_crumbs    = $this->_tax_crumbs( $term, 'self' );
                             $parent_tit = sprintf( $linkpatt, get_permalink($post->post_parent), get_the_title($post->post_parent) );
                             $_out = implode( $sep, array($_crumbs, $parent_tit) );
                             $out = $this->_add_title( $_out, $post );
                         }
                     }
                 }
                 // single
                 elseif( is_single() ){
                     if( ! $out = apply_filters('post_tax_crumbs', '', $term, $this ) ){
                         $_crumbs = $this->_tax_crumbs( $term, 'self' );
                         $out = $this->_add_title( $_crumbs, $post );
                     }
                 }
                 // не древовидная такса (метки)
                 elseif( ! is_taxonomy_hierarchical($term->taxonomy) ){
                     // метка
                     if( is_tag() )
                         $out = $this->_add_title('', $term, sprintf( $loc->tag, esc_html($term->name) ) );
                     // такса
                     elseif( is_tax() ){
                         $post_label = $ptype->labels->name;
                         $tax_label = $GLOBALS['wp_taxonomies'][ $term->taxonomy ]->labels->name;
                         $out = $this->_add_title('', $term, sprintf( $loc->tax_tag, $post_label, $tax_label, esc_html($term->name) ) );
                     }
                 }
                 // древовидная такса (рибрики)
                 else {
                     if( ! $out = apply_filters('term_tax_crumbs', '', $term, $this ) ){
                         $_crumbs = $this->_tax_crumbs( $term, 'parent' );
                         $out = $this->_add_title( $_crumbs, $term, esc_html($term->name) );                     
                     }
                 }
             }
             // влоежния от записи без терминов
             elseif( is_attachment() ){
                 $parent = get_post($post->post_parent);
                 $parent_link = sprintf( $linkpatt, get_permalink($parent), esc_html($parent->post_title) );
                 $_out = $parent_link;
 
                 // вложение от записи древовидного типа записи
                 if( is_post_type_hierarchical($parent->post_type) ){
                     $parent_crumbs = $this->_page_crumbs($parent);
                     $_out = implode( $sep, array( $parent_crumbs, $parent_link ) );
                 }
 
                 $out = $this->_add_title( $_out, $post );
             }
             // записи без терминов
             elseif( is_singular() ){
                 $out = $this->_add_title( '', $post );
             }
         }
 
         // замена ссылки на архивную страницу для типа записи
         $home_after = apply_filters('kama_breadcrumbs_home_after', '', $linkpatt, $sep, $ptype );
 
         if( '' === $home_after ){
             // Ссылка на архивную страницу типа записи для: отдельных страниц этого типа; архивов этого типа; таксономий связанных с этим типом.
             if( $ptype && $ptype->has_archive && ! in_array( $ptype->name, array('post','page','attachment') )
                 && ( is_post_type_archive() || is_singular() || (is_tax() && in_array($term->taxonomy, $ptype->taxonomies)) )
             ){
                 $pt_title = $ptype->labels->name;
 
                 // первая страница архива типа записи
                 if( is_post_type_archive() && ! $paged_num )
                     $home_after = sprintf( $this->arg->title_patt, $pt_title );
                 // singular, paged post_type_archive, tax
                 else{
                     $home_after = sprintf( $linkpatt, get_post_type_archive_link($ptype->name), $pt_title );
 
                     $home_after .= ( ($paged_num && ! is_tax()) ? $pg_end : $sep ); // пагинация
                 }
             }
         }
 
         $before_out = sprintf( $linkpatt, home_url(), $loc->home ) . ( $home_after ? $sep.$home_after : ($out ? $sep : '') );
 
         $out = apply_filters('kama_breadcrumbs_pre_out', $out, $sep, $loc, $arg );
 
         $out = sprintf( $wrappatt, $before_out . $out );
 
         return apply_filters('kama_breadcrumbs', $out, $sep, $loc, $arg );
     }
 
     function _page_crumbs( $post ){
         $parent = $post->post_parent;
 
         $crumbs = array();
         while( $parent ){
             $page = get_post( $parent );
             $crumbs[] = sprintf( $this->arg->linkpatt, get_permalink($page), esc_html($page->post_title) );
             $parent = $page->post_parent;
         }
 
         return implode( $this->arg->sep, array_reverse($crumbs) );
     }
 
     function _tax_crumbs( $term, $start_from = 'self' ){
         $termlinks = array();
         $term_id = ($start_from === 'parent') ? $term->parent : $term->term_id;
         while( $term_id ){
             $term       = get_term( $term_id, $term->taxonomy );
             $termlinks[] = sprintf( $this->arg->linkpatt, get_term_link($term), esc_html($term->name) );
             $term_id    = $term->parent;
         }
 
         if( $termlinks )
             return implode( $this->arg->sep, array_reverse($termlinks) ) /*. $this->arg->sep*/;
         return '';
     }
 
     // добалвяет заголовок к переданному тексту, с учетом всех опций. Добавляет разделитель в начало, если надо.
     function _add_title( $add_to, $obj, $term_title = '' ){
         $arg = & $this->arg; // упростим...
         $title = $term_title ? $term_title : esc_html($obj->post_title); // $term_title чиститься отдельно, теги моугт быть...
         $show_title = $term_title ? $arg->show_term_title : $arg->show_post_title;
 
         // пагинация
         if( $arg->pg_end ){
             $link = $term_title ? get_term_link($obj) : get_permalink($obj);
             $add_to .= ($add_to ? $arg->sep : '') . sprintf( $arg->linkpatt, $link, $title ) . $arg->pg_end;
         }
         // дополняем - ставим sep
         elseif( $add_to ){
             if( $show_title )
                 $add_to .= $arg->sep . sprintf( $arg->title_patt, $title );
             elseif( $arg->last_sep )
                 $add_to .= $arg->sep;
         }
         // sep будет потом...
         elseif( $show_title )
             $add_to = sprintf( $arg->title_patt, $title );
 
         return $add_to;
     }
 
 }
///////////////////////////////////////////////////////////////////////////////////////////////////



function mayak_nav_menu_no_link($no_link){
	$gg_mk = '!<li(.*?)class="(.*?)current-menu-item(.*?)"><a(.*?)>(.*?)</a>!';
	$dd_mk = '<li$1class="\\2menu-item\\3"><span aria-current="page" class="nav-link" >$5</span>';
	return preg_replace($gg_mk, $dd_mk, $no_link );
	}
	add_filter('wp_nav_menu', 'mayak_nav_menu_no_link');


    add_action('pre_get_posts', 'get_posts_search_filter');
    function get_posts_search_filter( $query ){
        if ( ! is_admin() && $query->is_main_query() && $query->is_search ) {
            $query->set('post_type', array('post', 'cars') );
        }
    }
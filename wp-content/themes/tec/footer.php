<?php wp_footer(); ?>
<footer class="footer_dark background_bg overlay_bg_80" data-img-src="assets/images/footer_bg.jpg">
	<div class="footer_top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center">
                	<div class="widget">
                        <div class="footer_logo">
                            <?php if (is_front_page() || is_home() ) : ?>
                                <span>
                                    <img class="logo_footer" src="<?php echo get_template_directory_uri();?>/assets/images/logo_white_2.png" alt="logo">
                                </span>
                            <?php else : ?>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" >
                                    <img class="logo_footer" src="<?php echo get_template_directory_uri();?>/assets/images/logo_white_2.png" alt="logo">
                                </a>
                            <?php endif; ?>
                        </div>
                        <h3>Electric cars are the future</h3>
                    </div>
                    <div class="widget">
                        <ul class="social_icons social_white social_style1 rounded_social">
                            <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                            <li><a href="#"><i class="ion-social-youtube-outline"></i></a></li>
                            <li><a href="#"><i class="ion-social-instagram-outline"></i></a></li>
                        </ul>
                    </div>
        		</div>
            </div>
        </div>
    </div>
</footer>
    <a href="#" class="scrollup" style="display: none;"><i class="ion-ios-arrow-up"></i></a> 
</body>
</html> 
<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package E-cars.club
 */
	get_header();
?>

<main id="primary" class="site-main">
	<?php if ( have_posts() ) : ?>
	<header class="page-header">
		<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light fixed_bg"
			style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/search.jpg'?> )">
			<div class="container">
				<!-- STRART CONTAINER -->
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title">
							<h1 class="page-title"><?php esc_html_e( 'Searching results', 'tec' ); ?></h1>
						</div>
						<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="container py-5 pl-5">
		<div class="row">
			<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'search' );

				endwhile;
				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
		</div> 
		<div class='text-center'>
			<?php the_posts_navigation();?>
		</div>
	</div> 
</main>
<?php
	get_footer();
?>

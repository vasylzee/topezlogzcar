<?php
/**
 * The template for displaying 404 pages (not found)
 */
	get_header();
?>
<main id="primary" class="site-main">
	<section class="error-404 not-found">
		<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light"
		style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/404.jpg'?> )">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title">
						<h1>Error 404</h1>
						</div>
						<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="page-content text-center my-5">
			<h3 class="error404-content__descr">
				Page not found
			</h3>
			<p class="error404-content__text">
				Maybe you entered an incorrect URL, or the page you are looking for has been removed, moved to another section, or temporarily unavailable.
			</p>
			<p class="error404-content__text">
				You can return to the <a href="/" class="error404-link">main page</a> to start looking for the information you need.
			</p>
		</div>
	</section>
</main>
<?php
	get_footer();
?>

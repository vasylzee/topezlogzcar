<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TeC
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
	<div class="container">
		<div class="comment-area bg-grey">
		<?php
			if ( have_comments() ) :
			?>
			<h2 class="comments-title content_title text-center my-3">
				<?php
				$tec_comment_count = get_comments_number();
				if ( '1' === $tec_comment_count ) {
					printf(
						esc_html__( '1 comment on &ldquo;%1$s&rdquo;', 'tec' ),
						'<span>' . wp_kses_post( get_the_title() ) . '</span>'
					);
				} else {
					printf( 
						esc_html( _nx( '%1$s comments on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $tec_comment_count, 'comments title', 'tec' ) ),
						number_format_i18n( $tec_comment_count ), 
						'<span>' . wp_kses_post( get_the_title() ) . '</span>'
					);
				}
				?>
			</h2>
			<ul class="comment-list list_none comment_list">
				<?php
				wp_list_comments(
					array(
						'style'      => 'ul',
						'short_ping' => true,
						'avatar_size'=> 48,
						'echo'       => true,
						'reverse_top_level' => true 
					)
				);
				?>
			</ul>

			<?php
			the_comments_navigation();
			if ( ! comments_open() ) :
				?>
				<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'tec' ); ?></p>
				<?php
			endif;

		endif; 
		comment_form();
		?>
	</div>
</div>

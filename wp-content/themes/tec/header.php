<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="Templatemanja" name="author">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Tec">

<!-- SITE TITLE -->
<title>
<?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
<!-- Favicon Icon -->
<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png">
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet"> 
<?php wp_head() ?>

</head>
<body>
<?php wp_body_open(); ?>
<div class="loading-window" id='preloader'>
    <div class="car">
        <div class="strike"></div>
        <div class="strike strike2"></div>
        <div class="strike strike3"></div>
        <div class="strike strike4"></div>
        <div class="strike strike5"></div>
        <div class="car-detail spoiler"></div>
        <div class="car-detail back"></div>
        <div class="car-detail center"></div>
        <div class="car-detail center1"></div>
        <div class="car-detail front"></div>
        <div class="car-detail wheel"></div>
        <div class="car-detail wheel wheel2"></div>
    </div>
</div>
<header class="header_wrap fixed-top header_with_topbar dark_skin main_menu_uppercase">
    <div class="container">
        <nav class="navbar navbar-expand-lg"> 
            <?php if (is_front_page() || is_home() ) : ?>
                <span class="navbar-brand">
                <img class="logo_dark" src="<?php echo  get_template_directory_uri(); ?>/assets/images/logo_black_2.png" alt="logo">
                </span>
					<?php else : ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand">
                            <img class="logo_dark" src="<?php echo  get_template_directory_uri(); ?>/assets/images/logo_black_2.png" alt="logo">
					</a>
				<?php endif; ?>
                
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false"> 
            	<span class="ion-android-menu"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent"> 
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'top',
                        'menu_id'        => 'primary-menu',
                        'menu_class'     => 'menu navbar-nav',
                        'add_li_class'   => 'dropdown',  
                        'container' => false,                      
                    )
                );
                ?>
            </div>
            <ul class="navbar-nav attr-nav align-items-center menu ">
                <li><a href="javascript:void(0);" class="nav-link search_trigger"><i class="linearicons-magnifier"></i></a>
                    <div class="search_wrap">
                  
                    	<span class="close-search"><i class="ion-ios-close-empty"></i></span>
                            <?php get_search_form() ?>
                    </div><div class="search_overlay"></div>
                </li>
            </ul>
            </div>
        </nav>
    </div>
</header>
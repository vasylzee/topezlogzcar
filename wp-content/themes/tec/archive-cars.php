<?php
/* Template Name: Cars */
get_header();
?>
<main id="primary" class="site-main">
	<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light fixed_bg"
		style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/electric-car.jpg'?> )">
		<div class="container">
			<!-- STRART CONTAINER -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1> Cars</h1>
					</div>
					<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();  ?>
				</div>
			</div>
		</div>
	</div>
	<?php 
		do_shortcode('[mdf_search_form id="313"]');
	?>
	<div class="container mt-5 pb-5">
		<div class="row">
			<div class="col-md-9 pr-5">
				<div class="row justify-content-center filtered-posts animation" data-animation="fadeInUp" data-animation-delay="0.2s">
					<?php
						$current_page = !empty( $_GET['page'] ) ? $_GET['page'] : 1;
						$query = new WP_Query( array(
							'posts_per_page' => 6,
							'paged' => $current_page, 
							'post_type' => 'cars',
						) );
						while( $query->have_posts() ) : $query->the_post(); 
						
						get_template_part('template-parts/content', 'cars');
					
						endwhile;
					?>
				</div>
			</div>
			<div class="col-md-3">
				<?php get_sidebar('cars'); ?>
			</div>
		</div>
		<div class="cars-page__paginate text-center">
			<?php
				echo paginate_links( array(
					'base' => site_url() . '/cars/' .'%_%',
					'format' => '?page=%#%',
					'total' => $query->max_num_pages,
					'current' => $current_page,
				) );
			?>
		</div>
	</div>
</main>
<?php
	get_footer();
?>
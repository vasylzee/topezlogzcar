<?php get_header(); 

/* Template Name: About Us Template */

global $wp_query; ?>

<main class="main">
    <div class="breadcrumb_section background_bg overlay_bg_50 page_title_light fixed_bg"
		style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/about-us-bg.jpg'?> )">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1> <?php the_title() ?></h1>
					</div>
					<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
    <div class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                    <div class="about_section pl-lg-3">
                        <div class="heading_s1">
                            <h2>E-cars.club - information portal about electric cars. </h2>
                        </div>
                        <p>Here you will find an extensive catalog of car brands, reviews of new products, news in the field of environmentally friendly vehicles, announcements of events related to eco-transport, and more.</p>
                        <p>Automotive manufacturers have long been working to meet the growing demand for environmentally-friendly vehicles with electric models. Their main advantage is that they do not pollute the environment with exhaust gases, consume much fewer resources, and also have high-tech functionality, which increases the demand for them today.</p>
                    </div>
                </div>
                <div class="col-lg-6 animation" data-animation="flipInY" data-animation-delay="0.03s">	
                    <div class="about_double_img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/electrocar.png'?>" alt="about_img2" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section background_bg fixed_bg overlay_bg_50" data-img-src="<?php echo get_template_directory_uri() . '/assets/images/about-bg.jpg'?> ">
        <div class="container">
            <div class="row">
                <div class="animation heading_s1 heading_light col-12 text-center" data-animation="fadeInUp" data-animation-delay="0.02s">
                        <h2>Our mission</h2>
                </div>
            </div>
            <div class="row justify-content-center mt-5">
                <div class="col-lg-4 col-md-6 ">
                    <div class="icon_box  icon_box_style1 btn-white text-center animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                        <div class="icon">
                            <i class="fa fa-fire"></i>
                        </div>
                        <div class="icon_box_content p-3 ">
                            <p>Popularization of electric transport</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 ">
                    <div class="icon_box icon_box_style1 btn-white text-center animation" data-animation="fadeInUp" data-animation-delay="0.03s">
                        <div class="icon">
                            <i class="fab fa-pagelines"></i>
                        </div>
                        <div class="icon_box_content p-3">
                            <p>Caring for ecology and the environment</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="icon_box icon_box_style1 text-center animation" data-animation="fadeInUp" data-animation-delay="0.04s">
                        <div class="icon">
                            <i class="fas fa-charging-station"></i>
                        </div>
                        <div class="icon_box_content p-3">
                            <p>Involvement in the culture of energy-efficient, environmentally friendly, and affordable transport</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="icon_box icon_box_style1 text-center animation" data-animation="fadeInUp" data-animation-delay="0.04s">
                        <div class="icon">
                            <i class="fas fa-info"></i>
                        </div>
                        <div class="icon_box_content p-3">
                            <p>Informing in the field of electric cars</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="icon_box icon_box_style1 text-center animation" data-animation="fadeInUp" data-animation-delay="0.04s">
                        <div class="icon">
                            <i class="far fa-money-bill-alt"></i>
                        </div>
                        <div class="icon_box_content p-3">
                            <p>Contributing to the reduction of oil dependence</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="icon_box icon_box_style1 text-center animation" data-animation="fadeInUp" data-animation-delay="0.04s">
                        <div class="icon">
                            <i class="fas fa-microchip"></i>
                        </div>
                        <div class="icon_box_content p-3">
                            <p>Promotion of modern technologies</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 animation" data-animation="flipInY" data-animation-delay="0.03s">	
                    <div class="about_double_img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/ab_us_policy.png'?>" alt="about_img2" />
                    </div>
                </div>
                <div class="col-lg-6 animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                    <div class="about_section pl-lg-3">
                        <div class="heading_s1">
                            <h2>Our policy</h2>
                        </div>
                        <p>We do not advertise or promote specific car brands and manufacturers.  We collect, analyze and provide information on those car models that meet our and your requests for environmentally friendly, energy-efficient, and affordable transport</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php 
    get_footer();
?>
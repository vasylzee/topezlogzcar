<?php
/* Template Name: News Template */
global $query;
get_header();
?>

<main id="primary" class="site-main">
	<section class="blog">
		<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light "
			data-img-src="<?php the_field('news_background_img')?>">
			<div class="container">
				<!-- STRART CONTAINER -->
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title">
							<h1> <?php the_title()?></h1>
						</div>
						<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container py-5 pl-5">
			<div class="row">
				<div class="row col-md-9 justify-content-center ">
					<?php
						$current_page = !empty( $_GET['news'] ) ? $_GET['news'] : 1;

						$query = new WP_Query( array(
							'posts_per_page' => 4,
							'paged' => $current_page,
							'public' => true,
						) );
				
						while( $query->have_posts() ) : $query->the_post();
						?>
					<div class="col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
						<div class="blog_post blog_style1 box_shadow1">
							<div class="blog_img">
								<a href="<?php echo get_post_permalink($id) ?>">
									<?php the_post_thumbnail() ?>
								</a>
								<span class="post_date radius_all_10"><?php the_date('d M')?></span>
							</div>
							<div class="blog_content">
								<div class="blog_text">
									<ul class="list_none blog_meta d-flex justify-content-between">
										<li><i class="ti-user"></i> <?php the_author()?></li>
										<li><i class="ti-comments"></i> <?php echo get_comments_number() ?> Comment</li>
									</ul>
									<h5 class="blog_title"><a
											href="<?php echo get_post_permalink($id) ?>"><?php the_title()?></a></h5>
									<?php the_excerpt() ?>
								</div>
							</div>
						</div>
					</div> 
					<?php
						endwhile;
						?>
					<?php
						wp_reset_postdata(); 
						?>
				</div>
				<div class="col-md-3">
					<?php get_sidebar('news'); ?>
				</div>
			</div>
			<div class="news-navbar text-center">
				<?php 
					echo paginate_links( array(
						'base' => site_url() . '/news/' .'%_%',
						'format' => '?news=%#%',
						'total' => $query->max_num_pages,
						'current' => $current_page,
						'end_size'     => 2,
						'mid_size'     => 3,
						) );
					?>
			</div>
		</div>
	</section>
</main><!-- #main -->

<?php
	get_footer();
?>
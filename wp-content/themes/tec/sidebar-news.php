<aside class="sidebar">
    <div class="widget">
        <div class="pb-3"> 
        <h5 class="widget_title">Recent Posts</h5>
            <ul class="widget_recent_post">
					<?php 
				$posts = get_posts( array(
					'numberposts' => 3,
					'category'    => 0,
					'orderby'     => 'date',
					'order'       => 'DESC',
					'meta_key'    => '',
					'meta_value'  =>'',
					'post_type'   => 'post',
					'suppress_filters' => true, 
				) );

				foreach( $posts as $post ){
					setup_postdata($post);
                ?>
                <li>
                    <div class="post_footer">
                        <div class="post_img">
                            <a href="<?php echo get_post_permalink($id) ?>"> 
                                <?php the_post_thumbnail( array(80,70)) ?>
                            </a>
                        </div>
                        <div class="post_content">
                            <h6>
                                <a href="<?php echo get_post_permalink($id) ?>"><?php the_title()?></a>
                            </h6>
                            <p class="small m-0">
                                <?php echo get_the_date( 'd M Y', $post );?>
                            </p>
                        </div>
                    </div>
                </li>
				<?php
                    }
                    wp_reset_postdata(); 
                ?>
                </ul>
            </div>
            <div class="widget mt-5"> 
                    <div class="widget_recent_post">
                        <h5 class="widget_title">Categories </h5>
                        <ul>
                        <?php 
                        $categories = get_categories( [
                            'taxonomy'     => 'category',
                            'type'         => 'post',
                            'child_of'     => 0,
                            'parent'       => '',
                            'orderby'      => 'count',
                            'order'        => 'DESC ',
                            'hide_empty'   => 1,
                            'hierarchical' => 1,
                            'exclude'      => '',
                            'include'      => '',
                            'number'       => 5,
                            'pad_counts'   => false,
                        ] );
                        if( $categories )
                            foreach( $categories as $cat ) {
                                ?>
                            <li>
                               <a href="<?php echo get_category_link( $cat ); ?>">
                                <?php echo $cat->name ?>
                                </a>
                            </li>
                            <?php 
                            }
                            ?>
                            
                        </ul>
                    </div>
                </div>
                <div class="widget mt-5">
                    <div class="widget_recent_post">
                        <h5 class="widget_title">Tags </h5>
                        <div class='tagcloud'>
                        <?php
                            wp_tag_cloud( array(
                                'smallest'  => 12,
	                            'largest'   => 12,
                                'number'    => 4,
                                'separator'    => " / ",
                                'taxonomy' => array('post_tag'),
                                'orderby' => 'count',
                                'order' => 'DESC',
                            ) );
                        ?>
                </div>
            </div>
        </div>
    </div>
</aside>    


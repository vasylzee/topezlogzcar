<?php
/* Template Name: Technology Template */
get_header();

global $post;

?>
<main id="primary" class="site-main">
	<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light fixed_bg "
		data-img-src="<?php the_field('technology_background_img')?>">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1> <?php the_title() ?></h1>
					</div>
					<ol class="breadcrumb">
						<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' > '); ?>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-2 my-5">
				<div class="tab-style2">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="Benefits-tab" data-toggle="tab" href="#Benefits" role="tab"
								aria-controls="Benefits" aria-selected="true">Benefits</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="Engine-tab" data-toggle="tab" href="#Engine" role="tab"
								aria-controls="Engine" aria-selected="false">Engine</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="Charge-tab" data-toggle="tab" href="#Charge" role="tab"
								aria-controls="Charge" aria-selected="false">Charge</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="Pricing-tab" data-toggle="tab" href="#Pricing" role="tab"
								aria-controls="Pricing" aria-selected="false">Pricing</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="Service-tab" data-toggle="tab" href="#Service" role="tab"
								aria-controls="Service" aria-selected="false">Service</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-9 mb-5 align-items-center">
				<div class="tab-content shop_info_tab">
					<div class="tab-pane fade show active animation animated fadeInUp" id="Benefits" role="tabpanel"
						aria-labelledby="Benefits-tab">
						<div class="heading_s1 text-center">
							<h2 class="text-center">Benefits</h2>
						</div>
						<div class="row mt-5 align-items-center">
							<div class="col-2">
								<div class="d-flex justify-content-center align-items-center tec-benefit-img">
									<i class="fas fa-leaf"></i>
								</div>
							</div>
							<div class="col-10">
								<h5 class="tec-benefit-title">Sustainability</h5>
								<div class="tec-benefit-content">Eco-transport has a beneficial effect on the
									environment, reducing noise pollution and harmful emissions into the atmosphere, and
									improving the ecological state of cities with saturated traffic.</div>
							</div>
						</div>
						<div class="row mt-5 align-items-center">
							<div class="col-10">
								<h5 class="tec-benefit-title">Ergonomic</h5>
								<div class="tec-benefit-content">The body layout of electric vehicles allows you to
									increase the luggage compartment and interior space. Many electric cars
									traditionally have two trunks, one of which is under the hood.</div>
							</div>
							<div class="col-2">
								<div class="d-flex justify-content-center align-items-center tec-benefit-img">
									<i class="fas fa-car"></i>
								</div>
							</div>
						</div>
						<div class="row mt-5 align-items-center">
							<div class="col-2">
								<div class="d-flex justify-content-center align-items-center tec-benefit-img">
									<i class="fas fa-tachometer-alt"></i>
								</div>
							</div>
							<div class="col-10">
								<h5 class="tec-benefit-title">Transmission advantages</h5>
								<div class="tec-benefit-content">Electricity is cheaper and more profitable than fossil
									fuels, electric transmission allows to charge your vehicle at any convenient time,
									the life of an electric motor is much longer than that of an internal combustion
									engine.</div>
							</div>
						</div>
						<div class="row mt-5 align-items-center">
							<div class="col-10">
								<h5 class="tec-benefit-title">Safety</h5>
								<div class="tec-benefit-content">As an innovative type of mobility, e-transport has the
									most advanced security systems, and electric vehicles are safer due to their
									simplicity of design and minimal fire hazard.</div>
							</div>
							<div class="col-2">
								<div class="d-flex justify-content-center align-items-center tec-benefit-img">
									<i class="fas fa-user-shield"></i>
								</div>
							</div>
						</div>
						<div class="row mt-5 align-items-center">
							<div class="col-2">
								<div class="d-flex justify-content-center align-items-center tec-benefit-img">
									<i class="fas fa-tools"></i>
								</div>
							</div>
							<div class="col-10">
								<h5 class="tec-benefit-title">Simple service</h5>
								<div class="tec-benefit-content">A completely different approach to maintenance with a
									minimum amount of service work and ample opportunities for online updates of
									electric vehicle systems.</div>
							</div>
						</div>
						<div class="row mt-5 align-items-center">
							<div class="col-10">
								<h5 class="tec-benefit-title">Benefit of use</h5>
								<div class="tec-benefit-content">Electric vehicles offer completely new forms of
									mobility and additional income opportunities for electric vehicle owners.</div>
							</div>
							<div class="col-2">
								<div class="d-flex justify-content-center align-items-center tec-benefit-img">
									<i class="far fa-money-bill-alt"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade animation animated fadeInUp" id="Engine" role="tabpanel"
						aria-labelledby="Engine-tab">
						<div class="heading_s1 text-center">
							<h2 class="text-center">Engine</h2>
						</div>
						<div class="row mt-5">
							<img src="<?php the_field('engine_img'); ?>" alt="type engine" class="mx-auto">
							<div class="engine-container mt-5">
								<h5 class="engine-title">EV (BEV) - fully electric cars.</h5>
								<div class="engine-content">These cars are driven by the energy stored in a battery that
									powers an electric motor. These cars do not use any other fuel except electricity,
									do not produce harmful emissions, and are considered the most environmentally
									friendly transport today.</div>
							</div>
							<div class="engine-container mt-5">
								<h5 class="engine-title">HEV - hybrid vehicles.</h5>
								<div class="engine-content">A hybrid car is a lot like regular gasoline or diesel car,
									but its drive consists of an internal combustion engine and an integrated electric
									motor that provides a certain amount of power. A hybrid electric motor can drive a
									car at low speeds, so HEVs are typically zero-emission vehicles in city driving
									mode. The electric motor is powered by a built-in battery, which is charged while
									the fuel motor is running and by regenerative braking.</div>
							</div>
							<div class="engine-container mt-5">
								<h5 class="engine-title">PHEV - plug-in hybrid (plug-in) vehicles.</h5>
								<div class="engine-content">Plug-in hybrids differ from conventional hybrids in that
									their electric battery can be charged from the mains. Typically, PHEVs have a larger
									battery and provide a longer purely electric range. There are two types of plug-in
									hybrids - parallel, the operation of the electric drive of which is more closely
									similar to electric cars and electric vehicles with increased range. In this type of
									plug-in, the car is started by an electric motor powered directly from the battery,
									but the battery itself is charged while driving by an integrated fuel generator.
								</div>
							</div>
							<div class="engine-container mt-5">
								<h5 class="engine-title">FCEV - fuel cell vehicles.</h5>
								<div class="engine-content">In the FCEV, the electric motor is powered by a hydrogen
									fuel cell. In a fuel cell, hydrogen combines with oxygen obtained from the air. This
									process creates energy that powers the electric motor. The advantage of fuel cell
									electric vehicles is that they can be refueled like conventional vehicles, while
									still obtaining the typical range of fuel cell vehicles. </div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade animation animated fadeInUp" id="Charge" role="tabpanel"
						aria-labelledby="Charge-tab">
						<div class="heading_s1 text-center">
							<h2 class="text-center">Сharge</h2>
						</div>
						<h5 class="engine-title text-center m-3">How does an electric car work?</h5>
						<div class="row">
							<div class="engine-container col-md-6">
								<div class="engine-content">
									<p> In the FCEV, the electric motor is powered by a hydrogen
										fuel cell.</p>
									<p>In a fuel cell, hydrogen combines with oxygen obtained from the air. This
										process creates energy that powers the electric motor. </p>
									<p>The advantage of fuel cell electric vehicles is that they can be refueled like
										conventional vehicles, while
										still obtaining the typical range of fuel cell vehicles. </p>
								</div>
							</div>
							<img src="<?php the_field('сharge_image_type_engine'); ?>" alt="type engine"
								class="col-md-6">
							<div class="engine-container mt-5">
								<h5 class="engine-title text-center m-3">How do I charge an electric vehicle?</h5>
								<div class="engine-content">
									<p> Electric cars have three charging modes, which can be
										roughly divided into slow, accelerated, and fast.
										<p>
											The simplest and most affordable type of charging an electric vehicle is
											connecting
											to a standard household outlet. It usually takes 7-12 hours, which is often
											limited
											by the capacity of a conventional network, which is 2-3.5 kW. Despite the
											long
											process, charging from a household network is the most common type of
											recharging an
											electric vehicle with energy.</p>
										<p>
											The second type is accelerated charging (AC), in which the electric vehicle
											is
											connected to a more powerful source of energy: up to 7.4 kW for single-phase
											and up
											to 22 kW for 3-phase AC. At the same time, the charging speed here depends
											on the
											capacity of the electric vehicle charger. As a rule, the newer and more
											powerful the
											electric vehicle, the more capacious battery it has, the more powerful its
											charger.
											An example is one of the most popular electric vehicles, the Nissan Leaf,
											whose 24
											kWh battery can be charged in 4 hours with a 6.6 kW charger.</p>
								</div>
								<div class="row">
									<img src="<?php the_field('сharge_image_charge_station'); ?>" alt="type engine"
										class="col-md-6 mx-auto">
									<div class="engine-content col-md-6">
										<p>
											It is also said that some electric vehicles, for example, Renault ZOE, have
											a
											built-in inverter up to 43 kW, which means they can charge quickly from AC
											sources.
											The third type is fast charging with direct current (DC) with the ability to
											charge
											the battery up to 80% capacity in half an hour.

									</div>
									<div class="engine-content m-3">
										<p>In order to charge an electric vehicle with direct current, it must be equipped with a connector of the
											appropriate	standard (CCS Combo, CHAdeMO, Tesla DC Supercharger) other than slow and
											fast charging ports.
											At the moment, there is no single standard for charging electric cars, but
											its	creation is a priority in the industry.</p>
										</div>
								</div>
							</div>

							<div class="engine-container mt-5">
								<h5 class="engine-title">Batteries: basic information.</h5>
								<div class="engine-content">
									<p>The battery in an electric vehicle is the key and most expensive element of the
										whole structure. The battery, as a rule, weighs several hundred kilograms and is
										installed on the floor of an electric vehicle, which provides a low center of
										gravity, adding stability when cornering and accelerating.</p>
									<p>For ease of calculation, the battery capacity is usually measured in kWh. The
										capacity of the battery determines the main characteristic of electric cars -
										the power reserve, the size of which directly affects their cost.</p>
									<p>Loss of capacity is the main evidence of battery wear, its highest rates fall in
										the first few years of operation. High temperature especially affects the degree
										of battery degradation, therefore it is strongly not recommended to overheat the
										battery. Also, each battery of an electric car has a certain number of charging
										cycles.</p>
									<p>For the battery to wear out as slowly as possible, you need to follow a few
										simple rules for the daily use of the battery:</p>
									<div class="row">
										<img src="<?php the_field('сharge_image_battery'); ?>" alt="type engine"
										class="col-md-6">
										<ul type="circle" class="col-md-6 mt-5">
											<li>prevent overheating of the battery;</li>
											<li>monitor the charge level (do not discharge the battery to a minimum);</li>
											<li>do not let the electric vehicle sit idle.</li>
										</ul>
									</div>
									<p class="mt-3">
										Manufacturers of electric vehicles use batteries of different chemical
										composition, which can have different lifetimes. In the majority of cases, the
										warranty period for batteries set by the manufacturer is 8 years.
									</p>
								</div>
							</div>
						
						</div>
					</div>
					<div class="tab-pane fade animation animated fadeInUp" id="Pricing" role="tabpanel"
						aria-labelledby="Pricing-tab">
						<div class="heading_s1 text-center">
							<h2 class="text-center">Pricing</h2>
						</div>
						<div class="row">
							<div class="engine-container mt-3">
								<div class="price-content m-3">
									<p>The cost of an electric car fluctuates depending on the car brand, its model, and
										equipment, which makes it possible for users with different budgets to choose.
										Besides, the big advantage of this type of car is the use of electricity, since
										it is much cheaper than gasoline or diesel. Also, electric cars require less
										maintenance than an internal combustion engine (ICE), and this is very
										profitable.</p>
								</div>

							</div>
							<div class="price-content col-lg-6 mt-4">
								<p>States actively promote the transition of citizens to a new type of transport and
									provide various incentives in the form of government subsidies, privileges,
									discounts, and exemption from fuel surcharges. Due to these benefits, the
									purchase of electric vehicles is much cheaper. But, as soon as sales become more
									active and massive, the state will stop supporting citizens in this way.</p>

							</div>
							<img src="<?php the_field('price_img'); ?>" alt="type engine" class="col-lg-6">
							<div class="price-content m-3">
								<p>This is what is happening in the USA now. The subsidy here will be about $ 7.5
									thousand. When a car manufacturer sells 200 thousand cars, this discount begins
									to decrease several times every six months. For example, Tesla has crossed this
									mark, so we should expect a gradual rise in prices for cars of this brand, also,
									such car brands as Nissan and BMW are close to the mark.</p>
								<p>Nevertheless, discounts and benefits, combined with constantly falling prices,
									make the option of buying an electric vehicle very attractive and economically
									viable.</p>
								<p>According to experts' forecasts, shortly, electric cars should equalize in price
									with gasoline cars and, starting from 2024, they will begin to become cheaper.
									It is expected that by 2044, about 55% of all car sales will be electric cars.
								</p>
							</div>
						</div>
					</div>
					<div class="tab-pane fade animation animated fadeInUp" id="Service" role="tabpanel"
						aria-labelledby="Service-tab">
						<div class="heading_s1 text-center">
							<h2 class="text-center">Service</h2>
						</div>
						<h5 class="engine-title mt-4 text-center">Electric vehicle service</h5>
						<div class="row mt-4">
							<div class="engine-container col-md-6">
								<div class="engine-content">
									<p>If we are talking about EVs, not hybrids (plug-in) and fuel cell EVs, their ease
										of maintenance is one of the main advantages of owning.</p>
									<p>Electric cars do not have a clutch or gears in the traditional sense, and in
										general, the design of an electric car's transmission has fewer moving parts,
										among which there are no consumables familiar to motorists.</p>


								</div>
							</div>
							<img src="<?php the_field('service_image'); ?>" alt="Service electrocar" class="col-md-6">
							<div class="engine-content m-3">
								<p>Candles, oil change, filters - all this is not required here. As a rule,
									maintenance is limited to changing the brake fluid, oil in the gearbox, and
									antifreeze in the battery.</p>
								<p>The most important part of servicing an electric vehicle is diagnostics of the
									electric motor and battery cells.</p>
							</div>
							<div class="engine-container">
								<h5 class="engine-title text-center ">Electric car driving</h5>
								<div class="row mt-4 ">
									<img src="<?php the_field('service_image_drive'); ?>" alt="Service electrocar"
										class="col-md-6  mx-auto">
									<div class="engine-content col-md-6 ">
										<p>Driving an electric vehicle differs from driving a car with an internal
											combustion
											engine, primarily in that the electric motor almost immediately offers the
											driver
											all the torque.
										</p>
										<p> The second unusual nuance is the absence of the usual sound of a
											running engine and the quiet running of an electric car. The most unusual in
											this
											car is an opportunity to use the regenerative driving modes, which allow one
											pedal
											to drive an electric vehicle, such as the e-Pedal in the 2018 Nissan Leaf or
											L mode
											in the Chevrolet Bolt EV. </p>
									</div>
								</div>
								<div class="engine-content mt-4">
									<p>At first, you need to get used to them, but then moving
										around the city becomes more convenient, since you do not need to constantly
										press
										the brake pedal.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php
	get_footer();
?>
<?php
get_header();
global $post;
?>

<main id="primary" class="site-main">
<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light"
        data-img-src="<?php the_post_thumbnail_url() ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h1><?php the_title() ?></h1>
                    </div>
                    <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' > '); ?>
                </div>
            </div>
        </div>
    </div>
    <?php if( have_posts()) : ?>
    <?php while ( have_posts() ): the_post();
            
            get_template_part('template-parts/content', 'card-news');
            
        endwhile; ?>
    <?php endif; ?>
</main>
<?php
get_footer();
?>
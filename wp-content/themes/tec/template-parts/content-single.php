<article id="post-<?php the_ID(); ?>" <?php post_class('blog_post');?>>
    <div class="single-news section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="single_post">
                        <div class="blog_img">
                            <?php the_post_thumbnail() ?>
                        </div>
                        <div class="blog_content">
                            <div class="blog_text">
                                <h2 class="blog_title"><?php the_title() ?></h2>
                                <ul class="list_none blog_meta">
                                    <li><a href="#"><i class="ti-user"></i><?php wp_list_authors();?></a></li>
                                    <li><a href="#"><i class="ti-calendar"></i>
                                            <?php echo get_the_date( 'd M Y', $post );?></a></li>
                                    <li><a href="#"><i class="ti-comments"></i> <?php echo get_comments_number() ?>
                                            comment</a></li>
                                </ul>
                                <?php the_content() ?>
                            </div>
                        </div>
                        <div class="blog_post_footer">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-md-8 mb-3 mb-md-0">
                                        <?php the_terms( get_the_ID(), 'post_tag' , '', ' / ', '' )?>
                                </div>
                                <div class="col-md-4">
                                    <ul class="social_icons text-md-right">
                                        <li><a href="#" class="sc_facebook"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="#" class="sc_twitter"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="#" class="sc_google"><i class="ion-social-googleplus"></i></a></li>
                                        <li><a href="#" class="sc_youtube"><i
                                                    class="ion-social-youtube-outline"></i></a></li>
                                        <li><a href="#" class="sc_instagram"><i
                                                    class="ion-social-instagram-outline"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
						$next_post = get_next_post();
						$previous_post = get_previous_post();
					?>
                    <div class="post_navigation py-3 border-top border-bottom">
                        <div class="row align-items-center justify-content-between">
                            <div class="col-5">
                                <?php	if(! empty($previous_post)){ ?>
                                <a href="<?php echo get_permalink( $previous_post ); ?>">
                                    <div class="post_nav post_nav_prev">
                                        <i class="ti-arrow-left"></i>
                                        <span class="text-uppercase nav_meta">Previous Post</span>
                                    </div>
                                </a>
                                <?php }?>
                            </div>
                            <div class="col-2">
                                <?php get_site_url('/news'); ?>
                                <a href="<?php echo get_site_url() . '/news'?>" class="post_nav_home">
                                    <i class="ti-layout-grid2"></i>
                                </a>
                            </div>
                            <div class="col-5">
                                <?php	if(! empty($next_post)){ ?>
                                <a href="<?php echo get_permalink( $next_post ); ?>">
                                    <div class="post_nav post_nav_next">
                                        <i class="ti-arrow-right"></i>
                                        <span class="text-uppercase nav_meta">Next Post</span>
                                    </div>
                                </a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mt-3 mt-lg-0">
                    <div class="sidebar">
                        <div class="widget">
                    	<h5 class="widget_title">Recent Posts</h5>
                        <ul class="widget_recent_post">
                            <li>
                                <div class="post_footer">
                                    <div class="post_img">
                                        <a href="#"><img src="assets/images/letest_post1.jpg" alt="letest_post1"></a>
                                    </div>
                                    <div class="post_content">
                                        <h6><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h6>
                                        <p class="small m-0">April 14, 2018</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post_footer">
                                    <div class="post_img">
                                        <a href="#"><img src="assets/images/letest_post2.jpg" alt="letest_post2"></a>
                                    </div>
                                    <div class="post_content">
                                        <h6><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h6>
                                        <p class="small m-0">April 14, 2018</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post_footer">
                                    <div class="post_img">
                                        <a href="#"><img src="assets/images/letest_post3.jpg" alt="letest_post3"></a>
                                    </div>
                                    <div class="post_content">
                                        <h6><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h6>
                                        <p class="small m-0">April 14, 2018</p>
                                    </div>
                                </div>
                            </li>
                    	</ul>
                    </div>
                        <div class="widget">
                            <h5 class="widget_title">Categories</h5>
                            <ul class="widget_categories">
                                <li><a href="#"><span class="categories_name">Lifestyle</span><span
                                            class="categories_num">(7)</span></a></li>
                                <li><a href="#"><span class="categories_name">Design</span><span
                                            class="categories_num">(15)</span></a></li>
                                <li><a href="#"><span class="categories_name">Branding</span><span
                                            class="categories_num">(8)</span></a></li>
                                <li><a href="#"><span class="categories_name">Marketing</span><span
                                            class="categories_num">(16)</span></a></li>
                                <li><a href="#"><span class="categories_name">Creative</span><span
                                            class="categories_num">(12)</span></a></li>
                                <li><a href="#"><span class="categories_name">Lifestyle</span><span
                                            class="categories_num">(11)</span></a></li>
                            </ul>
                        </div>
                        <div class="widget">
                            <h5 class="widget_title">tags</h5>
                            <div class="tags">
                                <a href="#">General</a>
                                <a href="#">Design</a>
                                <a href="#">jQuery</a>
                                <a href="#">Branding</a>
                                <a href="#">Modern</a>
                                <a href="#">Blog</a>
                                <a href="#">Quotes</a>
                                <a href="#">Advertisement</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ( comments_open() || get_comments_number() ) {
                comments_template();
            } ?>
    </div>
</article>
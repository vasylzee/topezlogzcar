<?php 
    $dropdownBrand = wp_dropdown_categories('echo=0&show_option_all=Brands&taxonomy=car-brand&name=brand&hide_empty=true');
	$dropdownType  = wp_dropdown_categories('echo=0&show_option_all=Types&taxonomy=car-type&name=type&hide_empty=true');
	$dropdownYear  = wp_dropdown_categories('echo=0&show_option_all=Years&taxonomy=car-year&name=years&hide_empty=true');
    
?>
<form action="/filter" method="get" class='filter-form my-5'>
    <?php
        echo $dropdownBrand;
        echo $dropdownType;
        echo $dropdownYear;
    ?>
    <button type="submit" name="" class='filter-form-btn btn my-3'>Filter</button>
</form>
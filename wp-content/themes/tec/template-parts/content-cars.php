<article id="post-<?php the_ID(); ?>" class="col-lg-4 col-sm-6 animated fadeInUp " data-animation="fadeInUp" data-animation-delay="0.02s">
	<div class="single_product">
		<div class="menu_product_img">
			<?php the_post_thumbnail() ?>
			<div class="action_btn">
				<a href="<?php echo get_post_permalink($id) ?>"	class="btn btn-white">show</a>
			</div>
		</div>
		<div class="menu_product_info">
			<div class="title">
				<h5><a href="<?php echo get_post_permalink($id) ?>"><?php the_title(); ?></a></h5>
			</div>
			<?php the_excerpt(); ?>
		</div>
		<div class="menu_footer ">
			<?php 
				$shortcode = do_shortcode('[wpdrating metakey="all" show-lable=false show-count=false show-average=false itemprop=false]'); 
				echo $shortcode;
			?>
		</div>
	</div>
	<?php wp_reset_postdata(); ?>
</article>
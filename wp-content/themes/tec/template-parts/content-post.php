<article id="post-<?php the_ID(); ?>" class="col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
    <div class="blog_post blog_style1 box_shadow1">
		<div class="blog_img">
			<a href="<?php echo get_post_permalink($id) ?>">
				<?php the_post_thumbnail() ?>
			</a>
			<span class="post_date radius_all_10"><?php the_date('d M')?></span>
		</div>
		<div class="blog_content">
			<div class="blog_text">
				<ul class="list_none blog_meta d-flex justify-content-between">
					<li><i class="ti-user"></i> <?php the_author()?></li>
					<li><i class="ti-comments"></i> <?php echo get_comments_number() ?> Comment</li>
				</ul>
				<h5 class="blog_title"><a href="<?php echo get_post_permalink($id) ?>"><?php the_title()?></a></h5>
				<?php the_excerpt() ?>
			</div>
		</div>
	</div>
	<?php
		wp_reset_postdata(); 
	?>
</article>
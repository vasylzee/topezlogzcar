<article id="post-<?php the_ID(); ?>" <?php post_class('car_single');?>>
    <div class="container">
		<div class="row mt-5">
            <div class="col-lg-6 col-md-6 ">
                <div class="product-image">
                    <?php the_post_thumbnail()?>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="pr_detail">
                    <div class="product_description">
                        <h4 class="product_title"><?php the_title() ?></h4>
                        <div class="row align-items-center">
                            <div class=" col-9">
                                <span class="price ">                                  
                                    $<?php the_field('car_price')?>
                                </span>
                            </div>
                            <div class="rating_wrap col-3">
                                <?php echo do_shortcode('[wpdrating metakey="all" show-lable=false show-count=false show-average = false itemprop=true]'); 
                                ?>
                            </div>
                        </div>
                            <div class="product-meta d-flex justify-content-between">
                                <div>Brand: <?php the_terms( get_the_ID(), 'car-brand', '', ' / ', '' )?></div>
                                <div>Type: <?php the_terms( get_the_ID(), 'car-type', '', ' / ', '' )?></div>
                                <div>Year: <?php the_terms( get_the_ID(), 'car-year', '', ' / ', '' )?></div>
                            </div>
                            <div class="pr_desc mt-3">
                                <?php the_excerpt()?>
                        </div>
                        <div class="product_share ">
                            <span>Share:</span>
                                <ul class="social_icons">
                                    <li>
                                        <a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="nofollow">
                                            <i class="ion-social-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" rel="nofollow">
                                            <i class="ion-social-twitter"></i>
						                </a>
                                    </li>
                                    <li>	
                                        <a href="https://www.linkedin.com/shareArticle?url=<?php the_permalink();?>&title=<?=$post->post_title?>" target="_blank" rel="nofollow">
                                            <i class="ion-social-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
        	<div class="col-12">
            	<div class="tab-style4">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="Description-tab" data-toggle="tab" href="#Description" role="tab" aria-controls="Description" aria-selected="true">Description</a>
                      	</li>
                      	<li class="nav-item">
                        	<a class="nav-link" id="Additional-info-tab" data-toggle="tab" href="#Additional-info" role="tab" aria-controls="Additional-info" aria-selected="false">Additional info</a>
                      	</li>
                      	<li class="nav-item">
                        	<a class="nav-link" id="Reviews-tab" data-toggle="tab" href="#Reviews" role="tab" aria-controls="Reviews" aria-selected="false">Reviews (<?php echo get_comments_number() ?>)</a>
                      	</li>
                    </ul>
                	<div class="tab-content shop_info_tab my-5">
                      	<div class="tab-pane fade show active" id="Description" role="tabpanel" aria-labelledby="Description-tab">
                      	    <?php the_content()?>
                        </div>
                      	<div class="tab-pane fade" id="Additional-info" role="tabpanel" aria-labelledby="Additional-info-tab">
                        	<table class="table table-bordered">
                                <tr>
                                    <td>Weight</td>
                                    <td><?php the_field('car_weight')?> kg</td>
                                </tr>
                                <tr>
                                    <td>Dimensions</td>
                                    <td>16 x 22 x 123 cm</td>
                                </tr>
                        	</table>
                      	</div>
                      	<div class="tab-pane fade" id="Reviews" role="tabpanel" aria-labelledby="Reviews-tab">
                            <div class="review_form">
                                <?php 
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                } ?> 
                            </div>
                      	</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</article>
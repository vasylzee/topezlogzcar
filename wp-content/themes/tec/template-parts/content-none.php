<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TeC
 */
?>
<section class="no-results not-found">
	<header class="page-header mt-5">
	<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light fixed_bg"
		style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/search.jpg'?> )">
		<div class="container">
			<!-- STRART CONTAINER -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'tec' ); ?></h1>
					</div>
					<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
	</header><!-- .page-header -->
		<div class="container">
		<div class="page-content m-5">
			<?php
			if ( is_home() && current_user_can( 'publish_posts' ) ) :

				printf(
					'<p>' . wp_kses(
						/* translators: 1: link to WP admin new post page. */
						__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'tec' ),
						array(
							'a' => array(
								'href' => array(),
							),
						)
					) . '</p>',
					esc_url( admin_url( 'post-new.php' ) )
				);

			elseif ( is_search() ) :
				?>

				<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'tec' ); ?></p>
				<?php
				get_search_form();
				?> 
				<a href="/" class="btn btn-default my-5 ">main</a>
				<?php
			else :
				?>

				<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'tec' ); ?></p>
				<?php

			endif;
			?>
		</div>
	</div>
</section>

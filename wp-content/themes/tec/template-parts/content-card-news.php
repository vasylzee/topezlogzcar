<article id="post-<?php the_ID(); ?>" <?php post_class('blog_post');?>>
    <div class="single-news section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="single_post">
                        <div class="blog_img_single ">
                            <?php the_post_thumbnail() ?>
                        </div>
                        <div class="blog_content">
                            <div class="blog_text">
                                <h2 class="blog_title"><?php the_title() ?></h2>
                                <ul class="list_none blog_meta">
                                    <li><i class="ti-user text-danger"></i><?php wp_list_authors();?></li>
                                    <li><i class="ti-calendar text-danger"></i>
                                            <?php echo get_the_date( 'd M Y', $post );?></li>
                                    <li><i class="ti-comments text-danger"></i> <?php echo get_comments_number() ?>
                                            comment</li>
                                </ul>
                                <?php the_content() ?>
                            </div>
                        </div>
                        <div class="blog_post_footer">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-md-8 mb-3 mb-md-0">
                                    <?php the_terms( get_the_ID(), 'post_tag' , '', ' / ', '' )?>
                                </div>
                                <div class="col-md-4">
                                    <ul class="social_icons text-md-right">
                                        <li>
                                            <a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="nofollow">
                                                <i class="ion-social-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" rel="nofollow">
                                                <i class="ion-social-twitter"></i>
                                            </a>
                                        </li>
                                        <li>	
                                            <a href="https://www.linkedin.com/shareArticle?url=<?php the_permalink();?>&title=<?=$post->post_title?>" target="_blank" rel="nofollow">
                                                <i class="ion-social-linkedin"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
						$next_post = get_next_post();
						$previous_post = get_previous_post();
					?>
                    <div class="post_navigation py-3 border-top border-bottom">
                        <div class="row align-items-center justify-content-between">
                            <div class="col-5">
                                <?php	if(! empty($previous_post)){ ?>
                                <a href="<?php echo get_permalink( $previous_post ); ?>">
                                    <div class="post_nav post_nav_prev">
                                        <i class="ti-arrow-left"></i>
                                        <span class="text-uppercase nav_meta">Previous Post</span>
                                    </div>
                                </a>
                                <?php }?>
                            </div>
                            <div class="col-2">
                                <?php get_site_url('/news'); ?>
                                <a href="<?php echo get_site_url() . '/news'?>" class="post_nav_home">
                                    <i class="ti-layout-grid2"></i>
                                </a>
                            </div>
                            <div class="col-5">
                                <?php	if(! empty($next_post)){ ?>
                                <a href="<?php echo get_permalink( $next_post ); ?>">
                                    <div class="post_nav post_nav_next">
                                        <i class="ti-arrow-right"></i>
                                        <span class="text-uppercase nav_meta">Next Post</span>
                                    </div>
                                </a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <?php if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        } ?>
                </div>
                <div class="col-lg-3 ">
                 <?php get_sidebar('news') ?>
                </div>
            </div>
        </div>
    </div>
</article>
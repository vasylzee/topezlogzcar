<?php get_header(); 

/* Template Name: Main Template */

global $wp_query; ?>
<main class="main">
    <section class='first'>
        <div class="banner_section slide_medium staggered-animation-wrap parallax_bg overlay_bg_80"
            data-parallax-bg-image="<?php the_field('main_background_img')?> ">
            <div class="banner_slide_content">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-md-12 col-sm-12 text-center">
                            <div class="banner_content text_white">
                                <h1 class="staggered-animation" data-animation="fadeInUp"
                                    data-animation-delay="0.2s"><?php the_field('main_title')?></h1>
                                <p class="staggered-animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                    <?php the_field('main_subtitle')?></p>
                                    <?php 
						                get_template_part('template-parts/content', 'main-filter');
                                     ?>
                            </div>
                        </div>
                    </div>
                </div><!-- END CONTAINER-->
            </div>
        </div>
    </section>
    <section class="top__car section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="text-center animation pb-5 animated fadeInUp " data-animation="fadeInUp" data-animation-delay="0.02s">
                        <div class="section_title  mx-auto">
                            <?php the_field('top_car_title')?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
					<?php
						$current_page = !empty( $_GET['page'] ) ? $_GET['page'] : 1;
						
						$query = new WP_Query( array(
							'posts_per_page' => 8,
							'paged' => $current_page, 
                            'post_type' => 'cars',            
                            'orderby'     => 'comment_count',
                            'order'       => 'DESC',
            
						) );
						
						while( $query->have_posts() ) : $query->the_post(); 
							
						get_template_part('template-parts/content', 'cars-main');
					

						endwhile;
			
					?>
                
                </div>
            </div>
    </section>
    <section class="opinion section parallax_bg overlay_bg_60 slide_medium"
        data-parallax-bg-image="<?php the_field('opinion_bg_img')?>">
        <div class="container ">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-lg-6 col-md-8 animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                    <div class="section_title_white heading_light">
                            <?php the_field('opinion_title')?>
                    </div>
                    <p class="text-white">
                        <?php the_field('opinion_subtitle')?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- START SECTION why_us -->
    <div class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                    <div class="section_title mb-md-0 text-center">
                        <?php the_field('why_us_title')?>
                    </div>
                    <div class="small_divider clearfix"></div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-9 animation" data-animation="fadeInUp" data-animation-delay="0.03s">
                    <div class="testimonial_wrap radius_all_10 text-center">
                        <div class="testimonial_slider testimonial_style3 slick_slider nav_style1" data-slides-to-show="1" data-slides-to-scroll="1" data-as-nav-for=".slick-thumb" data-autoplay="true" data-infinite="true" data-arrows="false">
                            <div class="testimonial_box">
                                <div class="card_name">
                                    <p>Our mission</p>
                                </div>
                                <div class="testimonial_desc">
                                    <p>Combine our knowledge of electric vehicles and existing solutions in the car market within one site. We do not sell cars, we are interested in truthful reviews on them. We strive to ensure that you receive the necessary and useful information that will help you when choosing a green car.</p>
                                </div>
                            </div>
                            <div class="testimonial_box">
                                <div class="card_name">
                                    <p>Our goal</p>
                                </div>
                                <div class="testimonial_desc">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem elit amet laudantium, quaeillo inventore sed veritatis et quasi architecto beatae vitae dicta sunt explicabo eiusmod tempor incididunt ut labore which toil and pain can procure him some great pleasure.</p>
                                </div>
                            </div>
                            <div class="testimonial_box">
                                <div class="card_name">
                                    <p>Our principles</p>
                                </div>
                                <ul class="testimonial_desc px-5 text-left">
                                    <li>We do not partner with companies for advertising purposes.</li>
                                    <li>We provide only truthful reviews.</li>
                                    <li>We take all published information responsibly.</li>
                                 </ul>
                            </div>
                            <div class="testimonial_box">
                                <div class="card_name">
                                    <p>Our knowledge</p>
                                </div>
                                <div class="testimonial_desc">
                                    <p>We are big fans of ecological cars and we believe that this is the transport of the future. From the very beginning of the development of this industry, we follow everything that happens and study the innovations and technologies used in the development of electric machines. We have already gained enough experience to share information and knowledge, so you can safely rely on us!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION why_us -->
    <!-- START SECTION COUNTER -->

    <section class="section parallax_bg overlay_bg_70 counter-section slide_medium"
        data-parallax-bg-image="<?php the_field('counter_bg_img') ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="section_title_white text-center animation col-12" data-animation="fadeInUp" data-animation-delay="0.2s">
                            Electric
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.2s">
                            <div class="box_counter counter_white text-center">
                                <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/co2-cloud.svg" alt="co2" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="0" data-speed="500"
                                        data-refresh-interval="1"></span></h3>
                                <p>per mile</p>
                            </div>
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.3s">
                            <div class="box_counter counter_white text-center">
                                <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/distance.svg" alt="distance" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="100" data-speed="1000"
                                        data-refresh-interval="10"></span> +/-</h3>
                                <p>mile range</p>
                            </div>
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.4s">
                            <div class="box_counter counter_white text-center">
                                <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/recycled.svg" alt="recycled" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="5" data-speed="500"
                                        data-refresh-interval="1"></span> +/-</h3>
                                <p>Hours to Recharge</p>
                            </div>
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.5s">
                            <div class="box_counter counter_white text-center">
                                <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/cent.svg" alt="cent" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="2" data-speed="500"
                                        data-refresh-interval="1"></span></h3>
                                <p>Cents per mile</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 my-sm-5 mt-md-0">
                    <div class="row">
                        <div class="section_title_white heading_light text-center animation col-12" data-animation="fadeInUp" data-animation-delay="0.2s">
                            vs
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="section_title_white heading_light text-center animation col-12" data-animation="fadeInUp" data-animation-delay="0.2s">
                            Gasoline
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.2s">
                            <div class="box_counter counter_white text-center">
                                <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/co2-cloud.svg" alt="co2" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="9" data-speed="500"
                                        data-refresh-interval="1"></span> </h3>
                                <p>+/- per mile</p>
                            </div>
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.3s">
                            <div class="box_counter counter_white text-center">
                            <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/distance.svg" alt="distance" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="300" data-speed="1000"
                                        data-refresh-interval="10"></span> +/-</h3>
                                <p>mile range</p>
                            </div>
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.4s">
                            <div class="box_counter counter_white text-center">
                                <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/refuel.svg" alt="refuel" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="5" data-speed="500"
                                        data-refresh-interval="1"></span> +/-</h3>
                                <p>Minutes to Refuel</p>
                            </div>
                        </div>
                        <div class="col-md-6 animation mt-5" data-animation="fadeInUp" data-animation-delay="0.5s">
                            <div class="box_counter counter_white text-center">
                                <img src="<?php echo  get_template_directory_uri(); ?>/assets/images/cent.svg" alt="cent" class="counter-icon">
                                <h3 class="counter_text"><span class="counter" data-from="0" data-to="12" data-speed="500"
                                        data-refresh-interval="1"></span>+</h3>
                                <p>Cents per mile</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </section>
    <!-- END SECTION COUNTER -->
    <section class="top_news">
        <div class="container">
            <div class="row justify-content-center pt-5 ">
                <div class="col-lg-6 col-md-8 animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                    <div class="section_title text-center">
                        Latest news
                    </div>
                    <p class="text-center leads">Follow the latest EV news</p>
                </div>
            </div>
            <div class="row justify-content-center py-3">
                <?php 
					// параметры по умолчанию
				$posts = get_posts( array(
					'numberposts' => 3,
					'category'    => 0,
					'orderby'     => 'date',
					'order'       => 'DESC',
					'meta_key'    => '',
					'meta_value'  =>'',
					'post_type'   => 'post',
					'suppress_filters' => true, 
				) );
				foreach( $posts as $post ){
					setup_postdata($post);
				?>
                <div class="col-lg-4 col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <div class="blog_post blog_style1 box_shadow1">
                        <div class="blog_img">
                            <a href="<?php echo get_post_permalink($id) ?>">
                                <?php the_post_thumbnail() ?>
                            </a>
                            <span class="post_date radius_all_10"><?php the_date('d M')?></span>
                        </div>
                        <div class="blog_content">
                            <div class="blog_text">
                                <ul class="list_none blog_meta d-flex justify-content-between">
                                    <li><i class="ti-user"></i> <?php the_author()?></li>
                                    <li><i class="ti-comments"></i> <?php echo get_comments_number() ?> Comment</li>
                                </ul>
                                <h5 class="blog_title"><a
                                        href="<?php echo get_post_permalink($id) ?>"><?php the_title()?></a></h5>
                                <?php the_excerpt() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
				}
				wp_reset_postdata();
				?>
            </div>
        </div>
    </section>
</main>

<?php 
    get_footer();
?>
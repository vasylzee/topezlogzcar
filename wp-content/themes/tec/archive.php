<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TeC
 */

get_header();

?>

<main id="primary" class="site-main">
	<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light "
		 style="background-image: url( <?php echo  get_template_directory_uri(); ?>/assets/images/car-bmw-i8.jpg)">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
					</div>
					<ol class="breadcrumb">
						<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' > '); ?>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<?php if ( have_posts() ) : ?>
			<div class="row mt-5 ">
		<div class="row justify-content-center col-md-9">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', get_post_type());
			endwhile;
			the_posts_navigation();
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif;
		?>
		</div>
			<div class="d-sm-none d-lg-block col-lg-3 pl-5">	
			<?php
				 if ( $post->post_type == "post"  ) {
					get_sidebar('news');
				} else if ( $post->post_type == "cars"  ){
					get_sidebar('cars');
				}
			?>
            </div>
		</div>
	</div>
</main><!-- #main -->

<?php
	get_footer();
?>
<?php /* Template Name: filter Template */ ?>

<?php get_header(); ?>
<?php 
    global $brandCars;
    global $typeCars;
    global $yearsCars;
    
?>
<?php 
        if($_GET['brand'] && !empty($_GET['brand'])){
            $brandCars = $_GET['brand'];
        }
        if($_GET['type'] && !empty($_GET['type'])){
            $typeCars = $_GET['type'];
        }
        if($_GET['years'] && !empty($_GET['years'])){
            $yearsCars = $_GET['years'];
        }
    ?>
<main id="primary" class="site-main">
	<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light fixed_bg"
		style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/filter_bg.jpg'?> )">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1>Filter</h1>
					</div>
					<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();  ?>
				</div>
			</div>
		</div>
    </div>
    <div class="container mt-5 pb-5">
        <?php 
            get_template_part('template-parts/content', 'main-filter');
            ?>
            <div class="row">
                <div class="col-md-9 pr-5">
                    <div class="row justify-content-center filtered-posts animation" data-animation="fadeInUp"
                        data-animation-delay="0.2s">
                        <?php
                        $args = array(
                            'post_type' => 'cars',
                            'posts_per_page' => -1,
                            'meta_query' => array(
                                array(
                                    'key' => 'brand',
                                    'value' => $brandCars,
                                    'compare' => 'LIKE'
                                ),
                                array(
                                    'key' => 'type',
                                    'value' => $typeCars,
                                    'compare' => 'LIKE'
                                ),
                                array(
                                    'key' => 'years',
                                    'value' => $yearsCars,
                                    'compare' => 'LIKE'
                                ),    
                            ),
                        );
                        $query = new WP_Query($args);
                        while($query -> have_posts()) : $query -> the_post();

                        get_template_part('template-parts/content', 'cars');
                        
                        endwhile; 
    
                        wp_reset_query(); ?>
                    </div>
                </div>
            <div class="col-md-3">
                <?php get_sidebar('cars'); ?>
            </div>
        </div>
    </div>
</main>

<?php 
    get_footer();
?>